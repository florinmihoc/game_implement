ALTER TABLE users
  ADD date_created DATE,
  ADD date_updated DATE,
  ADD warned BOOLEAN,
  ADD banned BOOLEAN,
  ADD token VARCHAR(100);
