CREATE TABLE public.users
(
    id SERIAL NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(100) NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.users
    OWNER to postgres;