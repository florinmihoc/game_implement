ALTER TABLE users
 ADD COLUMN nickname TEXT DEFAULT 'no name',
 ADD COLUMN validate BOOLEAN DEFAULT false;