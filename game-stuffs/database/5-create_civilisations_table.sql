CREATE TABLE public.civilisations
(
    id SERIAL NOT NULL,
    name text NOT NULL,
    description text NOT NULL
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.civilisations
    OWNER to postgres;

INSERT INTO civilisations (name, description)
    VALUES ('Nordici', 'Armata nordică este dominată de infanterie. În timp ce multe unități din infanteria celorlalte culturi țintesc cavaleria, anumite unități nordice de infanterie se confruntă cu alte tipuri de unități. Luptătotul cu toporul este o unitate de infanterie de scurtă durată care oferă prejudicii bonus altor unități de infanterie, iar Huskarl câștigă un bonus împotriva arcasilor.'),
           ('Greci', 'Grecii pot fi considerați ca având cea mai standardizată și ușor de memorat armată în joc. Fiecare clasă de unități umane - infanterie, arcaș și cavalerie - este produsă și modernizată de propria clădire specifică clasei. Grecii posedă unele dintre cele mai puternice unități de mitice în joc, dar ele sunt adesea costisitoare și pot fi greu de produs în masă fără a se concentra o sumă excesivă de săteni la venerarea zeilor pentru obținerea favorurilor.'),
           ('Egipteni', 'Muncitorii egipteni sunt mai lenți în colectarea resurselor decât lucrătorii altor culturi. Spre deosebire de celelalte culturi, egiptenii au clădiri separate pentru mâncare, lemn și aur; toate sunt construite mai încet. Egiptenii beneficiază de mai multe unitați mitice, favorurile fiind procurate prin construcția unui monument in cinstea zeului, acesta produce favoruri în mod constant în funcție nivel.');


CREATE TABLE public.villages
(
    id SERIAL NOT NULL,
    name text NOT NULL,
    user_id INTEGER NOT NULL
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.villages
    OWNER to postgres;

ALTER TABLE civilisations
 ADD COLUMN picture TEXT DEFAULT 'no picture';