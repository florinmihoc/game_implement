$(document).mouseup(function (e){

	var container = $("#user-menu");

	if (!container.is(e.target) && container.has(e.target).length === 0){

		container.fadeOut();
		
	}
});
$(document).ready(function() {

    var token = getCookie('user');
 
    callGetUserDataApi(function(user) {

        getUserVillagesApi(user.data.id, function(result) {
            if(result.status.code == 200) {
                $('#menu-nickname').text(user.data.nickname);
                $('#menu-email').text(user.data.email);

                $("#signout").click(function(e) {
                    e.preventDefault();
            
                    callLogoutApi(user.data.id);
                });
            } else {
                window.location.replace("/chooseCivilisation");
            }
        });
    });

    $("#menu-user-icon-button").click(function(e) { 
        var container = $("#user-menu");
        container.fadeIn();
    });

    function callLogoutApi(userId){
        $.ajax({
            type: "POST",
            url: "/api/user/logout",
            timeout: 2000,
            data: { 
                user_id: userId
            },
            success: function(json) {
                if(json.status.message == "LOGGED_OUT") {
                    document.cookie = 'user=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    window.location.href = '/';
                } else {
                    alert('Already logged out!');
                }
            },
            error: function(jqXHR, textStatus, err) {
                //show error message
                alert('text status '+textStatus+', err '+err)
            }
        });
    }

    function callGetUserDataApi(callback) {
         $.ajax({
            type: "POST",
            url: "/api/user",
            timeout: 2000, 
            data: { 
                token: token
            },
            success: function(json) {
                callback(json);
            },
            error: function(jqXHR, textStatus, err) {
                //show error message
                alert('text status '+textStatus+', err '+err)
            }
        });
    }

    function getUserVillagesApi(userId, callback) {
        $.ajax({
           type: "POST",
           url: "/api/user/getUserVillages",
           timeout: 2000, 
           data: { 
               user_id: userId
           },
           success: function(json) {
               callback(json);
           },
           error: function(jqXHR, textStatus, err) {
               //show error message
               alert('text status '+textStatus+', err '+err)
           }
       });
   }
});