$(document).ready(function() {
    var swiper = new Swiper('.swiper-container', {
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        loop: true,
        slidesPerView: 'auto',
        coverflowEffect: {
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows : true,
        },
        pagination: {
          el: '.swiper-pagination',
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });

    var token = getCookie('user');

    callGetUserDataApi(function(user) {
        getUserVillagesApi(user.data.id, function(result) {
            if(result.status.code == 200) {
                window.location.replace("/");
            } else {
                getCivilisations(function(result) {
                    var sliderIndex = 0;
                    var civilisation_id = result.data[sliderIndex].id;
                    $('.name').text(result.data[sliderIndex].name);
                    $('.description').text(result.data[sliderIndex].description);
                    swiper.on('slideChange', function () {
            
                        sliderIndex = swiper.realIndex;
                        civilisation_id = result.data[sliderIndex].id;
                        $('.name').text(result.data[sliderIndex].name);
                        $('.description').text(result.data[sliderIndex].description);
                      });
                      
                    $("#select").click(function(e) {
                        e.preventDefault();
                        var numeSat = $('#inp').val();
                        createVillage(token, civilisation_id, numeSat);
                    });
                });
            }
        });
    });

    function getCivilisations(callback) {
         $.ajax({
            type: "POST",
            url: "/api/user/getCivilizations",
            timeout: 2000, 
            data: {},
            success: function(json) {
                callback(json);
            },
            error: function(jqXHR, textStatus, err) {
                //show error message
                alert('text status '+textStatus+', err '+err)
            }
        });
    }

    function createVillage(token, civilisation_id, name) {
        $.ajax({
           type: "POST",
           url: "/api/user/createFirstVillage",
           timeout: 2000, 
           data: {
               token: token,
               civilisation_id: civilisation_id,
               name: name
           },
           success: function(json) {
               if(json.status.code == 200) {
                   window.location.replace("/");
               } else {
                   alert("Din pacate creearea satului a esuat va rugam incercati din nou!");
               }
           },
           error: function(jqXHR, textStatus, err) {
               //show error message
               alert('text status '+textStatus+', err '+err)
           }
       });
   }

   function callGetUserDataApi(callback) {
        $.ajax({
        type: "POST",
        url: "/api/user",
        timeout: 2000, 
        data: { 
            token: token
        },
        success: function(json) {
            callback(json);
        },
        error: function(jqXHR, textStatus, err) {
            //show error message
            alert('text status '+textStatus+', err '+err)
        }
    });
    }
   function getUserVillagesApi(userId, callback) {
        $.ajax({
        type: "POST",
        url: "/api/user/getUserVillages",
        timeout: 2000, 
        data: { 
            user_id: userId
        },
        success: function(json) {
            callback(json);
        },
        error: function(jqXHR, textStatus, err) {
            //show error message
            alert('text status '+textStatus+', err '+err)
        }
    });
    }
})