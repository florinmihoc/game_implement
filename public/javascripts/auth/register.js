$(document).ready(function() {
    $("#register_user").click(function(e) {
        e.preventDefault();

        var email = $("#email").val();
        var nickname = $("#nickname").val();
        var password = $("#password").val();
        var passConf = $("#passConf").val();

        if(email && password && nickname) {
            if(password == passConf) {
                callRegisterApi(email, password, nickname);
            } else {
                alert("Password did not mach!");
            }
        } else {
            alert("Please complete all fields!!");
        }
    });

    function callRegisterApi(bodyEmail, bodyPassword, bodyNickname) {
        $.ajax({
            type: "POST",
            url: "/api/user/register",
            timeout: 2000,
            data: { 
                email: bodyEmail, 
                password: bodyPassword,
                nickname: bodyNickname
            },
            success: function(json) {
                //show content
                if(json.status.message === "REQUEST_SENT") {
                    alert("your request was sent, we sent you a confirmation email!");
                } else {
                    alert(json.status.message);
                }
            },
            error: function(jqXHR, textStatus, err) {
                //show error message
                alert('text status '+textStatus+', err '+err)
            }
        });
    }
})