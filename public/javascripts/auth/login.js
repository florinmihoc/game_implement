$(document).ready(function() {
    $("#login_user").click(function(e) {
        e.preventDefault();

        var email = $("#email").val();
        var password = $("#password").val();

        if(email && password) {
            callLoginApi(email, password);
        } else {
            alert("Please complete both fields!!");
        }
    });

    function callLoginApi(bodyEmail, bodyPassword) {
        $.ajax({
            type: "POST",
            url: "/api/user/login",
            timeout: 2000,
            data: { 
                email: bodyEmail, 
                password: bodyPassword 
            },
            success: function(json) {
                //show content
                if(json.status.message === "LOGGED_IN") {
                    var date = new Date();
                    var expireTime = json.data.expires;

                    date.setTime(expireTime);

                    document.cookie = 'user=' + json.data.token + ';expires='+ date + ';path=/';
                    window.location.href = '/';
                } else {
                    alert(json.status.message);
                }
            },
            error: function(jqXHR, textStatus, err) {
                //show error message
                alert('text status '+textStatus+', err '+err)
            }
        });
    }
})