var moment = require('moment');
var winston = require('winston');

var dateHelper = require('../helpers/date');

module.exports = {
    checkParams: checkParams,
    validateToken: validateToken,
    logError: logError
};

function checkParams(message, code, data, res) {
    if (!data && data === undefined && !res.headersSent) {
        return res.json({
            status: {
                    'code': code,
                    'message': message
                }
            });
    }
}

function validateToken(req, res, callback) {
    try {
        var tokenDate = req.query.exp;
        var curentDate = moment().valueOf();

        if (curentDate <= tokenDate) {
            if (req.query.dbToken.trim() !== req.query.token.trim()) {
                return res.json({
                    status: {
                            'code': 502,
                            'message': 'INVALID_TOKEN_USER'
                        }
                    });
            } else {
                return callback(1);
            }
        } else {
            if (req.query.web == 1) {
                callback(0);
            } else {
                return res.json({
                    status: {
                            'code': 506,
                            'message': 'TOKEN_EXPIRED'
                        }
                });
            }
        }
    } catch (err) {
        console.log(err);
        logError(err);
        return res.json({
            status: {
                    'code': 501,
                    'message': 'INVALID_TOKEN'
                }
        });
    }
}

function logError(err) {
    var today;

    dateHelper.getDate(function(date){
        today = date;
    });
    // create a log only per month
    today.substring(0, today.lastIndexOf('-'));

    var logger = new winston.Logger({
            transports: [
                new winston.transports.File({
                    json: false,
                    level: 'info',
                    filename: './logs/' + today  + '.log',
                    handleExceptions: true,
                    maxsize: 5242880, //5MB
                    maxFiles: 5,
                    colorize: false
                })
            ],
            exitOnError: false
        });

        logger.info(err.stack + '\n################################################################################');
}