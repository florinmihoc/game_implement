var validator = require('validator');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var moment = require('moment');
var jwt = require('jwt-simple');

var constants = require('../config/constants');
var validationController = require('../controllers/validation');
var userModel = require('../models/user');
var emailController = require('../controllers/emails');
var urlConfig = require('../config/url');
var env = require('../env');

module.exports = {
    registerUser: registerUser,
    userLogin: userLogin,
    userLogout: userLogout,
    getUserData: getUserData,
    getCivilisations: getCivilisations
}

function registerUser(req, res, next) {
    try {
        validationController.checkParams('MISSING_EMAIL', 420, req.body.email, res);
        validationController.checkParams('MISSING_PASSWORD', 421, req.body.password, res);
        validationController.checkParams('MISSING_NICKNAME', 424, req.body.nickname, res);
    
        req.query.email = req.body.email;
    
        if(validator.isEmail(req.body.email)) {
            userModel.getUserByEmail(req, function(exist) {
                if(!exist) {
                    req.query.password = req.body.password;
                    var hash = bcrypt.hashSync(req.body.password);
                    req.body.password = hash;
                    req.body.date_created = Math.floor(Date.now() / 1000);
                    req.body.date_updated = Math.floor(Date.now() / 1000);
                    req.body.warned = false;
                    req.body.banned = false;
            
                    userModel.insertUser(req, res, function(result) {
                        res.json({
                            status: {
                                'code': 200,
                                'message': 'REQUEST_SENT'
                            }
                        });
                        var data = {
                            email: req.body.email,
                            url: urlConfig.url.emailConfirmation
                        };
                        emailController.compile(req, req.app.get('frontendEmailsPath'), 'confirmationEmail', data, function(err, html) {
                            if (err) {
                                console.log(err);
                                return new Error('Problem compiling template');
                            }
                            emailController.sendMail( env.options.fromAddress, req.body.email, 'Confirmation Email', html, function(err, success) {
                                if (err) {
                                    console.log(err);
                                    next(err);
                                }
                            });
                        });
                    });
                } else {
                    return res.json({
                        status: {
                            'code': 606,
                            'message': 'EMAIL_EXISTS'
                        }
                    });
                }
            });
        } else {
            return res.json({
                'code': 503,
                'message': 'INVALID_EMAIL_FORMAT'
            });
        }
    } catch (err) {
        validationController.logError(err);
        return res.json({
            status: {
                'code': 501,
                'message': 'INVALID_TOKEN'
            }
        });
    }
}

function userLogin(req, res, next){
    try {
        validationController.checkParams('MISSING_EMAIL', 420, req.body.email, res);
        validationController.checkParams('MISSING_PASSWORD', 421, req.body.password, res);

        req.query.email = req.body.email;
        var hash = '';

        if(validator.isEmail(req.body.email)) {
            userModel.getUserByEmail(req, function(userData) {
                if(userData) {
                    req.query.password = req.body.password;
                    hash = userData.password;
                    
                    var valid = bcrypt.compareSync(req.query.password, hash);
                    
                    if(valid && valid !== undefined) {
                        var expire = moment().add(constants.constant.tokenExpiration, 'days').valueOf();
                        var token = jwt.encode({
                            exp: expire,
                            email: req.body.email                        
                        }, req.app.get('jwtTokenSecret'));
                        if(userData.validate) {
                            req.body.date_updated = Math.floor(Date.now() / 1000);
                            userModel.setToken(token, req, function(updated) {
                                if(updated) {
                                    return res.json({
                                        'status': {
                                            'code': 200,
                                            'message': 'LOGGED_IN'
                                        },
                                        'data': {
                                            'token': token,
                                            'expires': expire,
                                            'user_id': userData.id
                                        }
                                    });
                                }
                            });
                        } else {
                            return res.json({
                                status: {
                                    'code': 425,
                                    'message': 'ACCOUNT_NOT_VALID'
                                }
                            });
                        }
                    } else {
                        return res.json({
                            status: {
                                'code': 423,
                                'message': 'INVALID_PASSWORD'
                            }
                        });
                    }
                } else {
                    return res.json({
                        status: {
                            'code': 422,
                            'message': 'NOT_REGISTERED'
                        }
                    });
                }
            });
        } else {
            return res.json({
                status: {
                    'code': 503,
                    'message': 'INVALID_EMAIL_FORMAT'
                }
            });
        }
    } catch (err) {
        validationController.logError(err);
        return res.json({
            status: {
                'code': 501,
                'message': 'INVALID_TOKEN'
            }
        });
    }
}
function userLogout(req, res, next) {
    try {
        validationController.checkParams('MISSING_USER_ID', 426, req.body.user_id, res);

        req.query.user_id = req.body.user_id;

        userModel.getUserById(req, res, function(exist) {
            if(exist.token != '') {
                userModel.userLogout(req, res, function (result) {
                    if(result) {
                        return res.json({
                            'status': {
                                'code': 200,
                                'message': 'LOGGED_OUT'
                            }
                        });
                    }
                });
            } else {
                return res.json({
                    status: {
                        'code': 504,
                        'message': 'ALREADY_LOGOUT'
                    }
                });
            }
        });
    } catch (err) {
        validationController.logError(err);
        return res.json({
            status: {
                'code': 501,
                'message': 'INVALID_TOKEN'
            }
        });
    }
}
function getUserData(req, res, next) {
    try {
        validationController.checkParams('MISSING_TOKEN', 501, req.body.token, res);

        req.query.token = req.body.token;

        var decoded = jwt.decode(req.query.token, req.app.get('jwtTokenSecret'));

        req.query.email = decoded.email;
        req.query.expire = decoded.exp;

        userModel.getUserByEmail(req, function(result) {
            if(result) {
                return res.json({
                    'status': {
                        'code': 200,
                        'message': 'USER_RETURNED'
                    },
                    'data': result
                });
            } else {
                return res.json({
                    status: {
                        'code': 501,
                        'message': 'INVALID_TOKEN'
                    }
                });
            }
        });
        
    } catch (err) {
        validationController.logError(err);
        return res.json({
            status: {
                'code': 501,
                'message': 'INVALID_TOKEN'
            }
        });
    }
}
function getCivilisations(req, res, next) {
    try {
        userModel.getCivilisations(req, res, function(result) {
            if(result) {
                return res.json({
                    'status': {
                        'code': 200,
                        'message': 'CIVILISATIONS_RETURNED'
                    },
                    'data': result
                });
            } else {
                return res.json({
                    status: {
                        'code': 501,
                        'message': 'NO_RESULT'
                    }
                });
            }
        });
        
    } catch (err) {
        validationController.logError(err);
        return res.json({
            status: {
                'code': 501,
                'message': 'INVALID_TOKEN'
            }
        });
    }
}