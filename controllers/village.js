var validator = require('validator');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var moment = require('moment');
var jwt = require('jwt-simple');

var constants = require('../config/constants');
var validationController = require('../controllers/validation');
var userModel = require('../models/user');
var villageModel = require('../models/village');
var emailController = require('../controllers/emails');
var urlConfig = require('../config/url');
var env = require('../env');

module.exports = {
    createFirstVillage: createFirstVillage,
    getVillages: getVillages
}

function createFirstVillage(req, res, next) {
    try {
        validationController.checkParams('MISSING_TOKEN', 401, req.body.token, res);
        validationController.checkParams('MISSING_NAME', 434, req.body.name, res);
        validationController.checkParams('MISSING_CIVILISATION_ID', 435, req.body.civilisation_id, res);
    
        req.query.token = req.body.token;
        req.query.name = req.body.name;
        req.query.civilisation = req.body.civilisation_id;

        var decoded = jwt.decode(req.query.token, req.app.get('jwtTokenSecret'));

        req.query.email = decoded.email;
        req.query.expire = decoded.exp;

        userModel.getUserByEmail(req, function(exist) {
            if(exist) {
                req.body.user_id = exist.id;
                villageModel.createVillage(req, res, function(result) {
                    if(result) {
                        return res.json({
                            status: {
                                'code': 200,
                                'message': 'VILLAGE_CREATED'
                            }
                        });
                    }
                });
            } else {
                return res.json({
                    status: {
                        'code': 607,
                        'message': 'EMAIL_NOT_EXISTS'
                    }
                });
            }
        });
    } catch (err) {
        validationController.logError(err);
        return res.json({
            status: {
                'code': 501,
                'message': 'INVALID_TOKEN'
            }
        });
    }
}
function getVillages(req, res, next) {
    try {
        validationController.checkParams('MISSING_USER_ID', 401, req.body.user_id, res);

        villageModel.getVillages(req, res, function(result) {
            if(result) {
                return res.json({
                    'status': {
                        'code': 200,
                        'message': 'VILLAGES_RETURNED'
                    },
                    'data': result
                });
            } else {
                return res.json({
                    status: {
                        'code': 501,
                        'message': 'INVALID_TOKEN'
                    }
                });
            }
        });
    } catch (err) {
        validationController.logError(err);
        return res.json({
            status: {
                'code': 501,
                'message': 'INVALID_TOKEN'
            }
        });
    }
}