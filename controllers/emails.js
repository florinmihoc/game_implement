var nodemailer = require('nodemailer'); 
var jade = require('jade');

var env = require('../env');

module.exports = {
    sendMail: sendMail,
    compile: compile
};

function sendMail(fromAddress, toAddress, subject, content, next) {
    var smtpTransport = nodemailer.createTransport(env.options.smtp);

    var success = true;
    var mailOptions = {
        from: env.options.mailName + ' <' +fromAddress + '>',
        to: toAddress,
        replyTo: fromAddress,
        subject: subject,
        html: content
    };

    smtpTransport.sendMail(mailOptions, function(err, response) {
        if (err) {
            console.log(err);
            success = false;
        }
        next(err, success);
    });
}

function compile(req, path, templateName, data, next) {
    var absoluteTemplatePath =  path + templateName +'.jade';

    jade.renderFile(absoluteTemplatePath, data, function(err, compiledTemplate) {
        if (err) {
            console.log(err);
            next(err);
        } 

        next(null, compiledTemplate);
    });
}