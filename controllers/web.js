var jwt = require('jwt-simple');

var validationController = require('../controllers/validation');

var usersModel = require('../models/user');

module.exports = {
    resetPasswordForm: resetPasswordForm,
    confirmEmail: confirmEmail
};

function resetPasswordForm(req, res, next) {
    req.query.token = req.params.token;

    try {
        var decoded = jwt.decode(req.query.token, req.app.get('jwtTokenSecret'));
        req.query.exp = decoded.exp;
        req.query.email = decoded.email;

        usersModel.getToken(req, res, function(result) {
            req.query.web = 1;
            validationController.validateToken(req, res, function(valid) {
                if (!res.headersSent && valid) {
                    res.render('users/forgotPasswordForm', {title: 'Forgot Password', email: req.query.email});
                } else {
                    res.render('users/tokenExpiredForm', {title: 'Your password has been changed successfully!', email: req.body.email});
                }
            });
        });
    } catch (err) {
        validationController.logError(err);
        return res.json({
            'status': {
                'code': 500,
                'message': 'INVALID_TOKEN'
            }
        });
    }
}

function confirmEmail(req, res, next) {
    req.query.email = req.params.email;

    try {
        usersModel.confirmEmail(req, res, function(confirmed) {
            if (confirmed) {
                res.redirect('/confirmationSucceded');
            } else {
                res.redirect('/confirmationFailed');
            }
        });
    } catch (err) {
        validationController.logError(err);
        return res.json({
            'status': {
                'code': 500,
                'error': err.message
            }
        });
    }
}