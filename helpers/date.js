module.exports = {
    getDate: getDate
};

function getDate(callback) {
    var date = new Date();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    date = year + '-' + month;

    return callback(date);
}