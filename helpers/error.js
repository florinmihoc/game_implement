module.exports = {
    errorResponse: errorResponse
};

function errorResponse(error, res) {
    return res.json({
        'status': {
            'code': 500,
            'message': error.message
        }
    });
}