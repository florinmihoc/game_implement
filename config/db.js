/*************************DB CONNECT - BEGIN **********************************/
var env = require('../env');

var promise = require('bluebird');

var options = {
    //Initialization Options
    promiseLib: promise
};

var pgp = require('pg-promise')(/*options*/);

var connectionString = 'postgres://' + env.options.dbUser + ':' 
                        + env.options.dbPassword + '@' + env.options.dbHost + ':'
                        + env.options.dbPort + '/' + env.options.dbName;
var db = pgp(connectionString);

module.exports.db = db;
                        
/*************************DB CONNECT - END ************************************/