var url = new Object();

url.apiUrl = 'http://localhost:3000';
url.resetPassword = url.apiUrl + '/user/forgot-password/';
url.emailConfirmation = url.apiUrl + '/user/email-confirmation/';

module.exports.url = url;