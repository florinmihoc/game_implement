var validationController = require('../controllers/validation');
var errorHelper = require('../helpers/error');
var database = require('../config/db');

module.exports = {
    insertUser: insertUser,
    getUserByEmail: getUserByEmail,
    setToken: setToken,
    confirmEmail: confirmEmail,
    getUserById: getUserById,
    userLogout: userLogout,
    getCivilisations: getCivilisations
}

function insertUser(req, res, callback) {
    var db = req.app.locals.db;

    if (req.body.token === undefined) {
        req.body.token = '';
    }

    db.none("INSERT INTO users (email, password, date_created, date_updated, banned, warned, token, nickname, validate)" +
            "VALUES (${email}, ${password}, to_timestamp(${date_created}), to_timestamp(${date_updated}), ${banned}, ${warned}, ${token}, ${nickname}, false)", req.body)
    .then(function(data) {
        req.body.password = req.query.password;
        callback(1);
    })
    .catch(function(err) {
        console.log(err);
        validationController.logError(err);
        return callback(err);
    });
}

function getUserByEmail (req, callback) {
    var db = req.app.locals.db;
    db.oneOrNone("SELECT * FROM users WHERE email = " + "'" + req.query.email + "'")
    .then(function(data){
        if(data != null) {
            return callback(data);
        } else {
            return callback(0);
        }
    })
    .catch(function(err) {
        console.log(err);
        validationController.logError(err);
    });
}       

function setToken(token, req, callback) {
    var db = req.app.locals.db;

    db.none("UPDATE users SET token = ($1), date_updated = to_timestamp($3) WHERE email = ($2)", [token, req.body.email, req.body.date_updated])
    .then(function(data) {
        return callback(1);
    })
    .catch(function(err) {
        console.log(err);
        validationController.logError(err);
        return callback(0);
    });
}

function confirmEmail(req, res, callback) {
    var db = req.app.locals.db;

    db.oneOrNone("UPDATE users " +
           "SET validate = true " +
           "WHERE email = ${email} " +
           "RETURNING validate", req.query)
    .then(function(data) {
        callback(data);
    })
    .catch(function(err) {
        console.log(err);
        validationController.logError(err);
        return errorHelper.errorResponse(err, res);
    });
}

function getUserById(req, res, callback) {
    var db = req.app.locals.db;

    db.any("SELECT * " +
           "FROM users " +
           "WHERE id = ${user_id} ", req.query)
    .then(function(data) {
        if(data != null) {
            return callback(data);
        } else {
            return callback(0);
        }
    })
    .catch(function(err) {
        console.log(err);
        validationController.logError(err);
        return errorHelper.errorResponse(err, res);
    });   
}

function userLogout(req, res, callback) {
    var db = req.app.locals.db;

    db.none("UPDATE users " +
           "SET token = '' " +
           "WHERE id = ${user_id} ", req.query)
    .then(function(data) {
        callback(1);
    })
    .catch(function(err) {
        console.log(err);
        validationController.logError(err);
        return errorHelper.errorResponse(err, res);
    });
}

function getCivilisations(req, res, callback) {
    var db = req.app.locals.db;

    db.any("SELECT * FROM civilisations")
    .then(function(data) {
        callback(data);
    })
    .catch(function(err) {
        console.log(err);
        validationController.logError(err);
        return errorHelper.errorResponse(err, res);
    });
}