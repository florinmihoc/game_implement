var validationController = require('../controllers/validation');
var errorHelper = require('../helpers/error');
var database = require('../config/db');

module.exports = {
    createVillage: createVillage,
    getVillages: getVilages
}
function createVillage(req, res, callback) {
    var db = req.app.locals.db;

    db.none("INSERT INTO villages (name, user_id, civilisation_id)" +
            "VALUES (${name}, ${user_id}, ${civilisation_id})", req.body)
    .then(function(data) {
        callback(1);
    })
    .catch(function(err) {
        console.log(err);
        validationController.logError(err);
        return callback(err);
    });
}
function getVilages(req, res, callback) {
    var db = req.app.locals.db;

    db.any("SELECT * FROM villages WHERE user_id = ${user_id}", req.body)
    .then(function(data) {
        if(data.length != 0) {
            callback(data)
        } else {
            callback(0);
        }
    })
    .catch(function(err) {
        console.log(err);
        validationController.logError(err);
        return callback(err);
    });
}