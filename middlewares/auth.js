var jwt = require('jwt-simple');

module.exports = {
    checkUserLogin: checkUserLogin
};

function checkUserLogin(req, res, next) {
    try{
        var cookie = req.cookies.user;
        var decoded = jwt.decode(cookie, req.app.get('jwtTokenSecret'));
        
        var expire = decoded.exp;
        var now = Date.now();

        if (req.cookies.user && (now > expire)) {
            res.redirect('/login');
        } else {
            next();
        }
    } catch (err) {
        res.redirect('/login');
    }
}