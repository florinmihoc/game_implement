/* 
 * Environment specific settings are declared here
 */
var options = new Object();
/********** DATABASE START ****************************************************/
options.dbHost = 'localhost'; // for example 'localhost';
options.dbName = 'game_implement'; // for example 'model_express_postresql'
options.dbUser = 'postgres'; // default is 'postgresql'
options.dbPassword = '1234';
options.dbPort = '5432'; // default is '5432'

/*************** EMAIL START **************************************************/
options.smtp = {
    host: 'smtp.gmail.com', // your smtp host (ex. smtp.gmail.com)
    port: 465, // 465 for SSL or 587 TLS
    secure: true, // require authentification
    auth: {
        user: 'florin.mihoc13@gmail.com', // your email (ex. your google mail) for SMTP login
        pass: 'comandor21' // your email password
    }
};
options.mailName = 'Alias'; // a name that can be used instead of the default email for sending mails
options.fromAddress = 'florin.mihoc13@gmail.com'; // the client's email address, which the user will see
options.devAddress = 'florin.mihoc13@gmail.com'; // the developer's email address, he will notified of the 500 internal errors
options.errorSubject = '[Project] - Error'; // the title that will be used to notify the devs
/*************** EMAIL END ****************************************************/

options.jwtTokenSecret = 'secret'; // will be used for in the token encryption algorithm

module.exports.options = options;