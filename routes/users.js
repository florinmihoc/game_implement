var express = require('express');
var bodyParser = require('body-parser')
var router = express.Router();
var authMiddleware = require('../middlewares/auth');


/////////////////////////////// CONTROLLERS ///////////////////////////////////
var usersController = require("../controllers/users");
var webController = require("../controllers/web");
var villageController = require("../controllers/village");
/////////////////////////////// END USER CONTROLLERS //////////////////////////

router.use(express.json());
router.use(express.urlencoded({ extended: true }));

/****************************** GET users listing. ***************************/

router.get('/', authMiddleware.checkUserLogin, function(req, res, next) {
  res.render('frontend/layouts/index', { title: 'Express' });
});

router.get('/login', function(req, res, next) {
  res.render('frontend/layouts/auth/login');
});

router.get('/register', function(req, res, next) {
  res.render('frontend/layouts/auth/register');
});

router.get('/confirmationSucceded', function(req, res, next) {
  res.render('frontend/layouts/user/confirmationSucceded',  {title: 'Successful Confirmation'});
});

router.get('/confirmationFailed', function(req, res, next) {
  res.render('frontend/layouts/user/confirmationFailed',  {title: 'Confirmation Failed'});
});

router.get('/chooseCivilisation', function(req, res, next) {
  res.render('frontend/layouts/user/chooseCivilisation');
});

/////////////////////////////// USER START /////////////////////////////////////
router.post('/api/user/register', usersController.registerUser);
router.post('/api/user/login', usersController.userLogin);
router.post('/api/user/logout', usersController.userLogout);
router.post('/api/user', usersController.getUserData);
router.post('/api/user/getCivilizations', usersController.getCivilisations);



/////////////////////////////// Village START /////////////////////////////////////
router.post('/api/user/createFirstVillage', villageController.createFirstVillage);
router.post('/api/user/getUserVillages', villageController.getVillages);

//////////////////////////////catch favicon 404 error //////////////////////////
//router.get('/favicon.ico', (req, res) => res.status(204));

/////////////////////////////// USER START /////////////////////////////////////
router.get('/user/email-confirmation/:email', webController.confirmEmail);

module.exports = router;